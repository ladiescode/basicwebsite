# BasicWebsite

** Bellow are the instructions to running this project **

---
## First you will need to get the source code 
## To clone the repository follow the steps bellow

1. Install Git
   Use these steps to clone from Gitbash, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

2. On Bitbucket, navigate to the main page of the repository.
      Clone or download buttonUnder the repository name, click Clone or download.

3. Clone URL buttonIn the Clone with HTTPs section, click  to copy the clone URL for the repository.

4. Open Git Bash.

5. Change the current working directory to the location where you want the cloned directory to be made.
   Type the following commands:
   * mkdir Projects
   * cd Projects
6. Type git clone, and then paste the URL you copied in Step 3.
  
    git clone https://github.com/YOUR-USERNAME/YOUR-REPOSITORY (Example of how the url looks like : https://magerine@bitbucket.org/ladiescode/basicwebsite.git)
   
    Press Enter. Your local clone will be created.

    git clone https://github.com/YOUR-USERNAME/YOUR-REPOSITORY
    Cloning into `basicwebsite`...
    remote: Counting objects: 10, done.
    remote: Compressing objects: 100% (8/8), done.
    remove: Total 10 (delta 1), reused 10 (delta 1)
    Unpacking objects: 100% (10/10), done.4. Open the directory you just created to see your repository�s files.

    Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

Than you are set to start building your own website